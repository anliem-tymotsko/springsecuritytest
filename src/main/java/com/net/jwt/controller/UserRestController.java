package com.net.jwt.controller;

import com.net.jwt.dto.UserDto;
import com.net.jwt.model.User;
import com.net.jwt.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/users/")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserRestController {
    private final UserService userService;

    @Autowired
    public UserRestController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping(value = "/all")
    public ResponseEntity<List<User>> getAllUsers(){
        return new ResponseEntity<>(userService.getAll(), HttpStatus.OK);
    }

    @GetMapping(value = "/current")
    public ResponseEntity<User> getCurrentUser(){
        return new ResponseEntity<>(userService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName()), HttpStatus.OK);
    }
}
