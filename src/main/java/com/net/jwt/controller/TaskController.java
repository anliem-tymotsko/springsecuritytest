package com.net.jwt.controller;

import com.net.jwt.model.Task;
import com.net.jwt.service.TaskService;
import com.net.jwt.service.UserService;
import com.net.jwt.service.implementation.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class TaskController {
    @Autowired
    private TaskService taskService;
    @Autowired
    MailService mailService;
    @Autowired
    UserService userService;

    @GetMapping("/api/tasks")
    ResponseEntity<List<Task>> getAll() {
        return ResponseEntity.ok(taskService.getAll());
    }

    @PostMapping("/api/task/create")
    public ResponseEntity create(@RequestBody Task task) {
        if (task != null) {
            taskService.create(task, SecurityContextHolder.getContext().getAuthentication().getName());
            return ResponseEntity.ok("created");
        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/api/task/update")
    public ResponseEntity<Task> update(@RequestBody Task task) {
        if (task != null) {

                return ResponseEntity.ok(taskService.update(task));

        } else
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/api/task/delete/{id}")
    public void deleteCompany(@PathVariable String id) {
        taskService.delete(id);
    }

    @GetMapping("/api/task/{id}")
    public Optional<Task> getById(@PathVariable String id) {
        return taskService.findById(id);
    }

    @GetMapping("/api/task/share/{userId}/{taskId}")
    public ResponseEntity shareTask(@PathVariable String userId, @PathVariable String taskId){
        taskService.share(userId, taskId, SecurityContextHolder.getContext().getAuthentication().getName());
        return new ResponseEntity(HttpStatus.OK);
    }



}
