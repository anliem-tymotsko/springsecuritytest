package com.net.jwt.repository;

import com.net.jwt.model.Task;
import com.net.jwt.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TaskRepo extends MongoRepository<Task, String> {
}
