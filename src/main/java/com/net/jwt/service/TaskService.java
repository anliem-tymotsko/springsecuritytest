package com.net.jwt.service;

import com.net.jwt.model.Task;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface TaskService {

    List<Task> getAll();

    void create(Task task, String username);

    Task update(Task task);

    void delete(String id);

    Optional<Task> findById(String id);

    void share(String id,String taskId, String sender);
}
