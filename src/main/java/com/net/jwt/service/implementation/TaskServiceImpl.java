package com.net.jwt.service.implementation;

import com.net.jwt.model.Task;
import com.net.jwt.model.User;
import com.net.jwt.repository.TaskRepo;
import com.net.jwt.repository.UserRepo;
import com.net.jwt.service.TaskService;
import com.net.jwt.service.UserService;
import com.sendgrid.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    TaskRepo taskRepo;
    @Autowired
    UserService userService;
    @Autowired
    UserRepo userRepo;
    @Autowired
    MailService mailService;

    @Override
    public List<Task> getAll() {
        return taskRepo.findAll();
    }


    @Override
    public void create(Task task, String username) {
        task.setCreated(new Date());
        User user = userService.findByUsername(username);
        task.setCreator(user);
        taskRepo.save(task);
        List<String> taskIDs = new ArrayList<>();
        if (user.getTasksID() != null) {
            taskIDs = user.getTasksID();
        }
        taskIDs.add(task.getId());
        user.setTasksID(taskIDs);
        userRepo.save(user);
    }

    @Override
    public Task update(Task task) {

        Task taskToUp = taskRepo.findById(task.getId()).get();
        taskToUp.setId(task.getId());
        taskToUp.setTitle(task.getTitle());
        taskToUp.setDescription(task.getDescription());

        taskRepo.save(taskToUp);
        return taskToUp;
    }

    @Override
    public void delete(String id) {
        taskRepo.deleteById(id);
    }

    @Override
    public Optional<Task> findById(String id) {
        return taskRepo.findById(id);
    }

    @Override
    public void share(String id, String taskId, String sender) {
        User user = userService.findById(id);
        List<String> taskIDs = new ArrayList<>();
        if (user.getTasksID() != null) {
            taskIDs = user.getTasksID();
        }
        taskIDs.add(id);
        user.setTasksID(taskIDs);
        userRepo.save(user);

        Email from = new Email(sender);
        Email to = new Email(user.getUsername());
        mailService.sendGrid(from, to, taskRepo.findById(taskId));
    }
}
