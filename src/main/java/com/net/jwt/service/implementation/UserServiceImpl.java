package com.net.jwt.service.implementation;

import com.net.jwt.model.Role;
import com.net.jwt.model.User;
import com.net.jwt.repository.UserRepo;
import com.net.jwt.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private  UserRepo userRepo;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepo userRepo, PasswordEncoder passwordEncoder) {
        this.userRepo = userRepo;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User register(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRole(Role.USER);
        user.setUsername(user.getEmail());
        User registerUser = userRepo.save(user);
        log.info("IN register - user: {} successfully registered", registerUser);
        return registerUser;
    }

    @Override
    public List<User> getAll() {
        List<User> allUsers = userRepo.findAll();
        log.info("IN all user found {} users", allUsers.size());
        return allUsers;
    }

    @Override
    public User findByUsername(String username) {
        User user = userRepo.findByUsername(username);
        log.info("IN found user: {} , by username: {}", user, username);
        return user;
    }

    @Override
    public User findById(String id) {
        User user = userRepo.findById(id);
        log.info("IN found user: {} BY ID: {}", user, id);
        return user;
    }

    @Override
    public void delete(Long id) {
    userRepo.deleteById(id);
    log.info("IN deleted user by id: {}", id);
    }
}
