package com.net.jwt.service.implementation;

import com.net.jwt.model.Task;
import com.sendgrid.*;

import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Optional;

@Service
public class MailService {


    public void sendGrid(Email from, Email to, Optional<Task> task) {
        String title = task.get().getTitle();
        String description = task.get().getDescription();

        Content content = new Content("text/plain", "HELLO! I want to share you this task : TITLE -" + title + ", DESCRIPTION - " + description);

        Mail mail = new Mail(from, "Sending msg", to, content);

        SendGrid sendGrid = new SendGrid("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");
        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());

            Response response = sendGrid.api(request);
            System.out.println(response.getStatusCode());
            System.out.println(response.getBody());
            System.out.println(response.getHeaders());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
