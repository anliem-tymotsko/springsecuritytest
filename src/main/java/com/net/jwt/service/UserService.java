package com.net.jwt.service;

import com.net.jwt.model.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {

    User register(User user);

    List<User> getAll();

    User findByUsername(String username);

    User findById(String id);

    void delete(Long id);

}
