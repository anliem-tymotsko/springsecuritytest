package com.net.jwt.security.jwt;

import com.net.jwt.model.User;


public final class JwtUserFactory {
    public JwtUserFactory() {
    }

    public static JwtUser create(User user) {
        return new JwtUser(
                user.getUsername(),
                user.getPassword(),
                user.getRole()
        );
    }
}
