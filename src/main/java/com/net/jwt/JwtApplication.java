package com.net.jwt;

import com.net.jwt.service.implementation.MailService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JwtApplication {

	public static void main(String[] args)
	{
		SpringApplication.run(JwtApplication.class, args);
	}

}
