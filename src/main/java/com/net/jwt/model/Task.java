package com.net.jwt.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
@Data
public class Task {

    @Id
    private String id;

    private String title;

    private String description;

    private Date created;

    private User creator;

}
