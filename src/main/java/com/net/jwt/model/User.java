package com.net.jwt.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
@Data
public class User {
    @Id
    private String id;

    private String fullName;

    private String username;

    private String email;

    private String password;

    private Role role;

    private List<String> tasksID;
}
